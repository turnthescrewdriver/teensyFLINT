#include "SdFat.h"
#include "ADC.h"
#include <IntervalTimer.h>
#include <Arduino.h>
#include <SPI.h>

//ADXL345 Register Addresses
#define	DEVID		0x00	//Device ID Register
#define THRESH_TAP	0x1D	//Tap Threshold
#define	OFSX		0x1E	//X-axis offset
#define	OFSY		0x1F	//Y-axis offset
#define	OFSZ		0x20	//Z-axis offset
#define	DURATION	0x21	//Tap Duration
#define	LATENT		0x22	//Tap latency
#define	WINDOW		0x23	//Tap window
#define	THRESH_ACT	0x24	//Activity Threshold
#define	THRESH_INACT	0x25	//Inactivity Threshold
#define	TIME_INACT	0x26	//Inactivity Time
#define	ACT_INACT_CTL	0x27	//Axis enable control for activity and inactivity detection
#define	THRESH_FF	0x28	//free-fall threshold
#define	TIME_FF		0x29	//Free-Fall Time
#define	TAP_AXES	0x2A	//Axis control for tap/double tap
#define ACT_TAP_STATUS	0x2B	//Source of tap/double tap
#define	BW_RATE		0x2C	//Data rate and power mode control
#define POWER_CTL	0x2D	//Power Control Register
#define	INT_ENABLE	0x2E	//Interrupt Enable Control
#define	INT_MAP		0x2F	//Interrupt Mapping Control
#define	INT_SOURCE	0x30	//Source of interrupts
#define	DATA_FORMAT	0x31	//Data format control
#define DATAX0		0x32	//X-Axis Data 0
#define DATAX1		0x33	//X-Axis Data 1
#define DATAY0		0x34	//Y-Axis Data 0
#define DATAY1		0x35	//Y-Axis Data 1
#define DATAZ0		0x36	//Z-Axis Data 0
#define DATAZ1		0x37	//Z-Axis Data 1
#define	FIFO_CTL	0x38	//FIFO control
#define	FIFO_STATUS	0x39	//FIFO status

#define BASE_FILE_NAME "log_"
#define FILE_EXTENSION_BIN "bin"
#define FILE_EXTENSION_CSV "csv"
#define BUF_DIM 12*1365*1 //16kB buffers seem to work the best.

const int readPeriod = 2000; //samplingRate in us

int CS=10; //Chip select signal for accelerometer
SPISettings adxl345settings(4000000, MSBFIRST, SPI_MODE3); //SPI settings for ADXL345
SdFatSdioEX sdEx; // SD Card volume
File file; // Binary logging file
File csv; // CSV file
ADC *adc = new ADC(); // ADC object
IntervalTimer timer0; // Timer object

char currentFileName[50]; // Holds the current file name of the binary logging file
uint16_t ADCReadings[3]; // Holds ADC samples
uint8_t upper; //holds MSbyte of ADC sample
uint8_t lower; // holds LSbyte of ADC sample
int16_t recon; // holds the reconstructed ADC sample
int colCounter=0;
uint8_t buff1[BUF_DIM]; //Buffer 1
uint8_t buff2[BUF_DIM]; //Buffer 2
uint8_t csvBuff[BUF_DIM]; //Binary data buffer when creating .csv file
uint8_t numBuffWritten=0; //Holds number of buffers written for current log
uint32_t buffPos1=0; //Holds index position of Buffer 1
uint32_t buffPos2=0; //Holds index position of Buffer 2
int whichBuff = 1; // Sets which buffer to stream ADC values to.
boolean buff1Full=false; //Controls whether buffer1 is full and can be written to SD.
boolean buff2Full = false; // Controls whether buffer2 is full can be written to SD.
boolean errorRaised = false; // Set to True if an error occurs.
int written = 0; //Used to hold how many bytes are written to file.
unsigned char accValues[10]; // Holds accelerometer values read from ADXL345
int16_t x,y,z;
double xg, yg, zg;

char * n;

// Writes to the registor on ADXL345
void writeRegister(char registerAddress, unsigned char value){
  //Set Chip Select pin low to signal the beginning of an SPI packet.
	digitalWrite(CS, LOW);
  //Set up SPI bus
  SPI.beginTransaction(adxl345settings);
	//Transfer the register address over SPI.
	SPI.transfer(registerAddress);
	//Transfer the desired register value over SPI.
	SPI.transfer(value);
	//Set the Chip Select pin high to signal the end of an SPI packet.
	digitalWrite(CS, HIGH);
  //End SPI transaction
  SPI.endTransaction();
}

// Reads a number of bytes from ADXL345 registers
void readRegister(char registerAddress, int numBytes, unsigned char * values){
  //Since we're performing a read operation, the most significant bit of the register address should be set.
  char address = 0x80 | registerAddress;
  //If we're doing a multi-byte read, bit 6 needs to be set as well.
  if(numBytes > 1)address = address | 0x40;

  //Set to correct mode
  SPI.beginTransaction(adxl345settings);
  //Set the Chip select pin low to start an SPI packet.
  digitalWrite(CS, LOW);
  //Transfer the starting register address that needs to be read.
  SPI.transfer(address);
  //Continue to read registers until we've read the number specified, storing the results to the input buffer.
  for(int i=0; i<numBytes; i++){
    values[i] = SPI.transfer(0x00);
  }
  //Set the Chips Select pin high to end the SPI packet.
  digitalWrite(CS, HIGH);

  //Ended
  SPI.endTransaction();
}

// Timer interrupt that runs every readPeriod microseconds
void timer0_callback(void){
        //Sample from ADC.
        ADCReadings[0]= adc->analogRead(A0);
        ADCReadings[1] = adc->analogRead(A1);
        ADCReadings[2] = adc->analogRead(A2);

        //Read X,Y,Z from accelerometer
        readRegister(DATAX0, 6, accValues);


        // Split ADC value into two bytes.
        for(int i=0; i<3; i++){
                upper = ADCReadings[i] >> 8; //MSB
                lower = ADCReadings[i] & 0x00ff; //LSB

                //Which buffer must get written to?
                if(whichBuff == 1)
                {
                        buff1[buffPos1] = upper; //MSB
                        buffPos1++; //increment index
                        buff1[buffPos1] = lower; //LSB
                        buffPos1++; //increment index

                        //If all ADC readings have been written,
                        //Write accelerometer data
                        if(i==2){
                          buff1[buffPos1] = accValues[0];
                          buffPos1++;
                          buff1[buffPos1] = accValues[1];
                          buffPos1++;
                          buff1[buffPos1] = accValues[2];
                          buffPos1++;
                          buff1[buffPos1] = accValues[3];
                          buffPos1++;
                          buff1[buffPos1] = accValues[4];
                          buffPos1++;
                          buff1[buffPos1] = accValues[5];
                          buffPos1++;
                        }
                }
                else if(whichBuff == 2) {
                        buff2[buffPos2] = upper;
                        buffPos2++;
                        buff2[buffPos2] = lower;
                        buffPos2++;

                        if(i==2){
                          buff2[buffPos2] = accValues[0];
                          buffPos2++;
                          buff2[buffPos2] = accValues[1];
                          buffPos2++;
                          buff2[buffPos2] = accValues[2];
                          buffPos2++;
                          buff2[buffPos2] = accValues[3];
                          buffPos2++;
                          buff2[buffPos2] = accValues[4];
                          buffPos2++;
                          buff2[buffPos2] = accValues[5];
                          buffPos2++;
                        }

                }

                //If the buffer is full
                if(buffPos1 == BUF_DIM) {
                        whichBuff=2; // Switch to other buffer for ADC storage.
                        buff1Full = true; //Set that buffer full and can be written to SD.
                        buffPos1 = 0; // Reset buffer position index.
                        Serial.println("Logging: BUFF1 --> Full.");
                        Serial.println("Logging: BUFF1 --> BUFF2");
                }
                else if(buffPos2 == BUF_DIM) {
                        whichBuff = 1;
                        buff2Full = true;
                        buffPos2 = 0;
                        Serial.println("Logging: BUFF2 --> Full");
                        Serial.println("Logging: BUFF2 --> BUFF1");
                }

        }

}

// Calculates the number of buffers written to file
int calcNumberOfBuffers(char * filename){
    File temp;
    int filesize=0;

    if(!temp.open(filename, O_READ)){
        Serial.println("ERROR OPENING FILE");
    }
    else{
        filesize = temp.fileSize(); //Get size of file
        return filesize/BUF_DIM; // Calculate number of written buffers
    }
    return -1;

}

// Gets the number appended to the end of a log file name
int getFileNumber(char * filename){
    char * n;
    char * num;
    int val;

    n = strtok(filename, "_.");
    num = strtok(NULL, "_."); // Holds the file number

    // If the number exists
    if(num != NULL){
        val = atoi(num); // Convert to int
        }

    return val; //Return file number
}

// Gets the extension and/or filetype of a file
char * getFileType(char * filename){
    char * n;
    char * ext;

    n = strtok(filename, ".");
    ext = strtok(NULL, "."); // Extracts the extension

    return ext;
}

// Returns a converted filename to the file type of choice
char * convertFileExtension(char * filename, const char * EXTENSION){
    char * n;
    char * r;
    n = strtok(filename, "."); //Get the filename preceding the extension
    r = (char *) malloc(20); // Allocate sufficient memory for new filename
    strcpy(r, n); // Copy filename to new filename
    strcat(r, "."); // Append period
    strcat(r, EXTENSION); // Append extension

    return r;
}

// Finds the latest file with a given extension, and increments the file number
// for the next file to be written.
void setNextFileName(const char * EXTENSION){
    File next;
    char name[20];
    char filenumber[10];
    char newName[50];
    char * ext;
    int pos_test=0;
    int pos_current=0;
    int filevalue=999;
    sdEx.vwd()->rewind(); //Go to begining of current working directory

    //While there is a next file in the directory
    while(next.openNext(sdEx.vwd(), O_READ)){
        next.getName(name,20); // get the name of the file
        ext = getFileType(name); // extract the file extension

        //If extension is of the type specified
        if(strcmp(EXTENSION, ext) == 0){
            // Get the file number
            pos_test = getFileNumber(name);

            // Finds the highest file number.
            // Note: This *coincidentally* works correctly even if no files
            // are on the card.
            if(pos_test > pos_current){
                pos_current = pos_test;
            }
        }

        next.close(); // Close each file to continue incrementing through volume
    }

    pos_current++; // Increment the extracted file number
    itoa(pos_current, filenumber, 10); // Convert to string
    strcpy(newName, BASE_FILE_NAME); // Create new file name from base

    // Do some formatting
    if(pos_current < 10){
        strcat(newName, "0"); // Append zero if single-digit file number
    }
    strcat(newName, filenumber); // Concatenate file number
    strcat(newName, "."); // Append period
    strcat(newName, EXTENSION); // Concatenate file extension

    strcpy(currentFileName, newName); // Copy to global variable for next file name
}


// Makes formatted .csv file for specified binary log file.
void makeCSV(char* filename)
{
        //nb: filename refers to the binary file!
        Serial.println("MakeCSV: Attempting to open binary file...");

        //Open binary log file
        if (!file.open(filename, O_READ))
        {
                Serial.println("MakeCSV: ERROR --> Unable to open binary file.");
        }
        else
        {
                file.rewind(); //return to start of file
                // Calculate the number of buffers written for the corresponding
                // binary file.
                numBuffWritten = calcNumberOfBuffers(filename);
                Serial.println("MakeCSV: SUCCESS --> Binary file opened.");
        }

        //Open and create .csv file
        char * csvFileName = convertFileExtension(filename, FILE_EXTENSION_CSV);
        if(!csv.open(csvFileName, O_WRITE | O_CREAT)) {
                Serial.println("MakeCSV: ERROR --> Unable to open .csv file");
        }
        else{
                Serial.println("MakeCSV: SUCCESS --> Opened .csv file");
                csv.truncate(0);
                Serial.println("MakeCSV: SUCCESS --> .csv cleared");
        }

        //Read in and reconstruct log file data to 16-bit integers and write to CSV.
        // Format:
        //         ADC1 MSB
        //          ADC1 LSB
        //          ADC2 MSB
        //          ADC2 LSB
        //          ADC3 MSB
        //          ADC3 LSB
        //          x-axis LSB
        //          x-axis MSB
        //          y-axis LSB
        //          y-axis MSB
        //          z-axis LSB
        //          z-axis MSB

        // For each buffer that has been written to SD.
        for(int j=0; j<numBuffWritten; j++) {

                // Read in a single buffer at a time.
                if(file.read(csvBuff, BUF_DIM) == -1) {
                        Serial.println("MakeCSV: ERROR --> Unable to read buffer from binary file");
                }
                else{
                        Serial.print("MakeCSV: Reading buffer ");
                        Serial.print(j+1);
                        Serial.print(" out of ");
                        Serial.println(numBuffWritten);
                        //Serial.println(".");
                }

                // Actual reconstruction of stored data
                for(size_t i=0; i<BUF_DIM; i++)
                {
                        int16_t adc1;
                        int16_t adc2;
                        int16_t adc3;

                        //ADC Values
                        adc1 = (csvBuff[i] << 8) | csvBuff[i+1];
                        i=i+2;
                        adc2 = (csvBuff[i] << 8) | csvBuff[i+1];
						i=i+2;
                        adc3 = (csvBuff[i] << 8 | csvBuff[i+1]);
                        i=i+2;

						// Accelerometer
						x = ((csvBuff[i+1] << 8) | csvBuff[i]);
						i=i+2;
						y = ((csvBuff[i+1] << 8) | csvBuff[i]);
						i=i+2;
						z = ((csvBuff[i+1] << 8) | csvBuff[i]);
                        i++; // Only add one to i, as loop will also i++

						//Convert from integer representation to Gs
						xg = x * 0.00390625*4;
						yg = y * 0.00390625*4;
						zg = z * 0.00390625*4;

                        //Write to CSV file
                        csv.print(adc1);
                        csv.print(",");
                        csv.print(adc2);
                        csv.print(",");
                        csv.print(adc3);
                        csv.print(",");
                        csv.print(x);
                        csv.print(",");
                        csv.print(y);
                        csv.print(",");
                        csv.print(z);
                        csv.println();

                }

                Serial.print("MakeCSV: Buffers written: ");
                Serial.print(j+1);
                Serial.print(" out of ");
                Serial.println(numBuffWritten);
        }
        file.close();
        csv.close();
        Serial.println("MakeCSV: Attempted write to .csv complete.");
}

// Converts all binary log files to .csv
void convertAllToCSV(){
    File next;
    char name[20];
    char fullname[20];
    char * newName;
    char * ext;
    int filenum;
    int counter = 1; // Used as a workaround

    // Since writing a new .csv file alters the position within the volume,
    // after writing each .csv file, the volume must be rewinded and stepped
    // through again. To prevent re-converting the previous file, a counter
    // is used. So the process becomes convert first file --> rewind volume
    // --> increment counter --> convert second file --> rewind volume, etc.

    sdEx.vwd()->rewind(); // Rewind to beginning of volume

    // While files exists in the volume
    while(next.openNext(sdEx.vwd(), O_READ)){
        next.getName(name, 20); // Get the filename
        next.getName(fullname, 20); // Gets second copy of filename
        ext = getFileType(name); // Get the file extension
        filenum = getFileNumber(name); // Get the file number

        // If a binary file, and matches the counter
        if(strcmp("bin", ext) == 0 && counter == filenum){
            Serial.print("CONVERSION: ");
            Serial.println(fullname);
            next.close();
            makeCSV(fullname); // Make .csv file
            counter++; // Increment counter
            sdEx.vwd()->rewind(); // Reset volume index
        }
        else{
            next.close(); // If not a binary file or matches counter, move on.
        }
    }
}

void setup() {

        //Set-up SPI first
        SPI.begin();
        pinMode(CS, OUTPUT);
        digitalWrite(CS, HIGH);

        //ADXL345 settings
        writeRegister(DATA_FORMAT, 0b00000010); //Set range to +-8g
        writeRegister(BW_RATE, 0b00001110); // set sampling rate to 1600Hz
        writeRegister(INT_ENABLE, 0x00); //disable interrupts
        writeRegister(POWER_CTL, 0x08); //Enable measurement mode
        readRegister(INT_SOURCE, 1, accValues); // Clear interrupts by reading interrupt register


        // ADC inputs
        pinMode(A0, INPUT);
        pinMode(A1, INPUT);
        pinMode(A2, INPUT);

        //ADC configuration
        adc->setAveraging(2);
        adc->setResolution(12);
        adc->setSamplingSpeed(ADC_SAMPLING_SPEED::MED_SPEED);
        adc->setConversionSpeed(ADC_CONVERSION_SPEED::MED_SPEED);
        delay(10);

        // Start serial
        Serial.begin(9600);
        delay(50);

        //init file system
        if (!sdEx.begin())
        {
                Serial.println("Begin function failed.");
        }

        //set current volume
        sdEx.chvol();

        Serial.println("Welcome to FLINT (Fast Logger that Isn't Terrible)");
        Serial.println("b to begin");
        Serial.println("s to stop");
        Serial.println("c to convert to .csv file");
}

void loop() {

        unsigned long t;
        char incoming = 0;

        // Read from serial
        if(Serial.available() > 0) {
                incoming = Serial.read();
        }

        if(incoming == 'b') {
                // Increment the file name to avoid overwriting logs
                setNextFileName(FILE_EXTENSION_BIN);
                Serial.print("Attempting to open new log file: ");
                Serial.println(currentFileName);

                delay(10);

                // Create new empty log
                if(!file.open(currentFileName, O_CREAT | O_WRITE)) {
                        Serial.println("ERROR OPENING LOG FILE.");
                        errorRaised = true;
                }
                else{
                        // Reset everything in preparation for fresh log
                        file.truncate(0);
                        numBuffWritten = 0;
                        buffPos1 = 0;
                        buffPos2 = 0;
                        whichBuff = 1;
                        buff1Full = false;
                        buff2Full = false;
                        Serial.println("Log file opened and cleared.");
                        Serial.println("READY TO SAMPLE");
                        errorRaised = false;
                }
                delay(100);
                // If there are no errors
                if(!errorRaised) {
                        timer0.begin(timer0_callback, readPeriod); // Begin timer
                        delayMicroseconds(10);
                        Serial.println("Timer started...");
                }
                else{
                        Serial.println("Error flag raised. Stopping.");
                }

        }
        // Stops logging
        else if(incoming == 's') {
                Serial.println("Stopping...");
                timer0.end(); // Stop timer
                delay(100);
                // Close files if open
                if(file.isOpen()) {
                        file.close();
                        Serial.println("Log file closed");
                }
                Serial.println("Giving time for files to be written to SD...");
                delay(2000); // Give some time for any files to be written to SD.
                Serial.println("Successfully stopped");
        }
        else if(incoming == 'c') {
                // Close files if open
                if(file.isOpen()) {
                        file.close();
                }
                delay(500);
                convertAllToCSV(); // Convert all files to .csv

        }

        // If buffer1 is full
        if(buff1Full)
        {
                t = millis();

                written = file.write(buff1, BUF_DIM); // Write buffer to binary log file
                if(written!=BUF_DIM)
                {
                        Serial.println(written);
                        Serial.println("Error: BUFF1 --> Write failed.");
                }
                else{
                        Serial.println("Success: BUFF1 --> Write success.");
                        Serial.print("Bytes written: ");
                        Serial.println(written);
                        numBuffWritten++;
                        buff1Full = false; // With buffer 1 written, can be used again.

                        Serial.print("Time elapsed: ");
                        Serial.println(millis() - t);
                        Serial.print("Buffers written: ");
                        Serial.println(numBuffWritten);
                }


        }
        else if(buff2Full) {
                //buff2Full = false;
                t = millis();

                written = file.write(buff2, BUF_DIM);

                if(written != BUF_DIM) {
                        Serial.println("Error: BUFF2 --> Write failed.");
                }
                else{
                        Serial.println("Success: BUFF2 --> Write success.");
                        Serial.print("Bytes written: ");
                        Serial.println(written);
                        numBuffWritten++;
                        buff2Full = false;

                        Serial.print("Time elapsed: ");
                        Serial.println(millis() - t);
                        Serial.print("Buffers written: ");
                        Serial.println(numBuffWritten);
                }
        }


}
